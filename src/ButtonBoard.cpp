#include "ButtonBoard.h"

ButtonBoard::ButtonBoard(byte aPin, int valArr[22][3]) {
	this->analogPin = aPin;										// Declare analod pin
	this->valueArray = valArr;								// Array with button values
}

int ButtonBoard::setButton() {

	int buttonID;

	// Read analog input value and return button ID
	int buttonValue = analogRead(analogPin);
	// Loop trough value array to find button ID matching analog values
	for(int i = 0; i < 22; i++) {
		if((buttonValue >= valueArray[i][1]) && (buttonValue <= valueArray[i][2])) {
			buttonID = valueArray[i][0];
		}
	}

	return buttonID;
}

// Debug function - print raw button values
void ButtonBoard::valueDBG() {
	int btn = analogRead(analogPin);
	Serial.print(F("Button Value: "));
	Serial.print(btn);
	Serial.println();
	delay(250);
}
