# ButtonBoard
ButtonBoard library and instructions to create and use navigation button board.\
Board consists of 6 buttons connected to one analog input on Arduino board (3 wires +5V, GND, AnalogInput), supporting button combination (up to 2 buttons)

## The library
Library depends on voltage drop using different count of resistors and combination of resistors. Expected values are configurable and passed as init parameters).\
Depend on the project analog input can be configured (also passed as init parameter)

## The board
Board is created with Perfboard (17x12 holes), 6 tactile switches (6mm) and 7 resistors (220, 390, 680, 1K, 2.2K, 4.7K, 7.4K). You can find detailed layout, schema, PCB and etch in the "extras" directory.
Example project can be used to print button assignments and current voltage drops in order to fine tune the board initialization
