#ifndef BUTTONBOARD_H
#define BUTTONBOARD_H

#include <Arduino.h>

class ButtonBoard {

  private:
    byte analogPin;
    int (*valueArray)[3];

  public:
    ButtonBoard(byte aPin, int valArr[22][3]);
    int setButton();
    void valueDBG();
};

#endif
